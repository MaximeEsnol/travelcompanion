/*
    Maxime Esnol
 */

import React from 'react';
import Constants from 'expo-constants';
import {StyleSheet, Text, View, ImageBackground, TextInput, Button, Alert} from 'react-native';

var background = require("./assets/barcelona.jpg");

class App extends React.Component {

    state = {
        inputDestination: "",
        destinations: [],
        destinationsVisited: []
    };

    showDestinations = () => {
        return this.state.destinations.map(
          d => {
            return(
                <Text style={styles.destination} key={d} onPress={() => {this.setToVisited(d)}}>
                    {d}
                </Text>
            )
          }
        )
    };

    showVisitedDestinations = () => {
        return this.state.destinationsVisited.map(
            d => {
                return(
                    <Text style={styles.visited} key={d}>
                        {d}
                    </Text>
                );
            }
        );
    };

    setToVisited = (d) => {
        var destinations = this.state.destinations;
        var visitedDestinations = this.state.destinationsVisited;
        var destinationIndex = destinations.indexOf(d);

        if(destinationIndex != -1){
            visitedDestinations.push(d);
            destinations.splice(destinationIndex, 1);
            this.setState({destinations: destinations, destinationsVisited: visitedDestinations});
            Alert.alert("Success!", "You've added " + d + " to your list of visited destinations.");
        } else {
            Alert.alert("Unknown destination", "We could not find this destination in your destinations list.");
        }
    };

    add = () => {
        var destination = this.state.inputDestination;
        var arr = this.state.destinations;
        arr.push(destination);
        this.setState({destinations: arr, inputDestination: ""});
    };

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={background} style={styles.headerImage} resizeMode="cover">
                    <View style={styles.header}>
                        <Text style={styles.title}>Travel Companion</Text>
                    </View>
                </ImageBackground>
                <View style={styles.content}>
                    <Text style={styles.heading2}>
                        <Text role="img" aria-label="plane">✈️</Text> New destination.
                    </Text>

                    <TextInput style={styles.input} placeholder="Where are you going to? " onChangeText={(text) => this.setState({inputDestination: text})}/>
                    <Button style={styles.button} onPress={this.add} title="Add"/>

                    <Text style={styles.heading2}>
                        <Text role="img" aria-label="island">🏝️</Text> Your destinations.
                    </Text>

                    {this.showDestinations()}

                    <Text style={styles.heading2}>
                        <Text role="img" aria-label="camera">📷</Text> Visited destinations.
                    </Text>

                    {this.showVisitedDestinations()}
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    headerImage: {
        width: "100%",
        height: "20%",
        paddingTop: Constants.statusBarHeight
    },
    header: {
        backgroundColor: "#00000080",
        width: "100%",
        height: "100%",
        alignItems: "center",
        justifyContent: "center"
    },
    heading2: {
        fontSize: 18,
        fontWeight: "600"
    },
    content: {
        padding: 10
    },
    input: {
        margin: 10,
        height: 40,
        borderWidth: 1,
        borderColor: "#545454"
    },
    button: {
        width: "5%"
    },
    title: {
        fontSize: 32,
        color: "white",
        fontWeight: "800"
    },
    destination: {
        borderBottomWidth: 1,
        borderBottomColor: "#ededed",
        marginTop: 10,
        marginBottom: 10,
        fontSize: 15
    },
    visited: {
        color: "#707070",
        textDecorationLine: "line-through",
        textDecorationStyle: "solid",
        borderBottomColor: "#ededed",
        marginTop: 10,
        marginBottom: 10,
        fontSize: 15
    }
});

export default App;
